
const $mainicon = document.getElementById("main")
const $introicon = document.getElementById("introduce")
const $projecticon = document.getElementById("project")
const $limg = document.getElementById('Ltitleimg');
const $rimg = document.getElementById('Rtitleimg');
const $icon = document.getElementById('icon');
const $phone = document.getElementById('phone');
const $email = document.getElementById('email');
const $git = document.getElementById('git');

$mainicon.addEventListener('click',()=>{location.href="./main.html";});
$mainicon.addEventListener('mouseover',()=>{
    $mainicon.style.backgroundImage = "url(resouces/image/Maintxt.png)";
    $mainicon.style.backgroundSize = "20%";
});
$mainicon.addEventListener('mouseout',()=>{
    $mainicon.style.backgroundImage = "url(resouces/image/Main.png)";
    $mainicon.style.backgroundSize = "15%";
});

$introicon.addEventListener('click',()=>{location.reload();});
$introicon.addEventListener('mouseover',()=>{
    $introicon.style.backgroundImage = "url(resouces/image/introducetxt.png)";
    $introicon.style.backgroundSize = "30%";
});
$introicon.addEventListener('mouseout',()=>{
    $introicon.style.backgroundImage = "url(resouces/image/introduce.png)";
    $introicon.style.backgroundSize = "15%";
});

$projecticon.addEventListener('click',()=>{location.href="https://gitlab.com/beginner-developer";});
$projecticon.addEventListener('mouseover',()=>{
    $projecticon.style.backgroundImage = "url(resouces/image/Projecttxt.png)";
    $projecticon.style.backgroundSize = "20%";
});
$projecticon.addEventListener('mouseout',()=>{
    $projecticon.style.backgroundImage = "url(resouces/image/Project.png)";
    $projecticon.style.backgroundSize = "15%";
});


$limg.addEventListener('click',()=>{
    $limg.classList.toggle('lani')
    
});
$rimg.addEventListener('click',()=>{
    $rimg.classList.toggle('rani')
    
});
$icon.addEventListener('click',()=>{
    $icon.animate({transform: 'rotate(360deg)'},1000)
    
});
    
$phone.addEventListener('mouseover',()=>{
    $phone.setAttribute("title","010-0000-000");
});
$phone.addEventListener('mouseout',() => {
    $phone.removeAttribute("title");
});

$email.addEventListener('mouseover',()=>{
    $email.setAttribute("title","cloverpig@greedy.com");
});
$email.addEventListener('mouseout',() => {
    $email.removeAttribute("title");
});

$git.addEventListener('mouseover',()=>{
    $git.setAttribute("title","https://gitlab.com/beginner-developer");
});
$git.addEventListener('mouseout',() => {
    $git.removeAttribute("title");
});